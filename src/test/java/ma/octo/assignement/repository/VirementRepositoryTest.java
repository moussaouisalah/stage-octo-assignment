package ma.octo.assignement.repository;

import ma.octo.assignement.model.Compte;
import ma.octo.assignement.model.Utilisateur;
import ma.octo.assignement.model.Virement;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class VirementRepositoryTest {

  @Autowired
  private VirementRepository virementRepository;
  @Autowired
  private CompteRepository compteRepository;
  @Autowired
  private UtilisateurRepository utilisateurRepository;

  private Compte compte1, compte2;

  @BeforeEach
  public void createMockData() {
    Utilisateur u1 = new Utilisateur("uEtest", "male", "test", "test", new Date());
    Utilisateur u2 = new Utilisateur("uBtest", "male", "test", "test", new Date());
    Utilisateur user1 = utilisateurRepository.save(u1);
    Utilisateur user2 = utilisateurRepository.save(u2);

    Compte c1 = new Compte("nrEtest", "ribEtest", new BigDecimal(50000), user1);
    Compte c2 = new Compte("nrBtest", "ribBtest", new BigDecimal(10000), user2);
    compte1 = compteRepository.save(c1);
    compte2 = compteRepository.save(c2);

  }

  @Test
  public void findOne() {
    Virement virementAjoute = virementRepository.save(
            new Virement(new BigDecimal(1000), new Date(), compte1, compte2, "transfert"));
    Optional<Virement> virementOptional = virementRepository.findById(virementAjoute.getId());

    Assertions.assertThat(virementOptional.isEmpty()).isFalse();
    Virement virementDB = virementOptional.get();
    Assertions.assertThat(virementDB).isSameAs(virementAjoute);
  }

  @Test
  public void findAll() {
    List<Virement> virementsAvant = virementRepository.findAll();
    int sizeAvant = virementsAvant.size();

    Virement v1 = virementRepository.save(
            new Virement(new BigDecimal(1000), new Date(), compte1, compte2, "transfert"));
    Virement v2 = virementRepository.save(
            new Virement(new BigDecimal(1500), new Date(), compte2, compte1, "transfert"));

    List<Virement> virements = virementRepository.findAll();
    Assertions.assertThat(virements).contains(v1, v2);
    Assertions.assertThat(virements.size()).isEqualTo(sizeAvant + 2);
  }

  @Test
  public void save() {
    Assertions.assertThat(compte1).isNotNull();
    Virement nouveauVirement = virementRepository.save(
            new Virement(new BigDecimal(2000), new Date(), compte1, compte2, "transfert"));
    Optional<Virement> virementOptional = virementRepository.findById(nouveauVirement.getId());

    Assertions.assertThat(virementOptional.isEmpty()).isFalse();
    Virement virementDB = virementOptional.get();
    Assertions.assertThat(virementDB).isSameAs(nouveauVirement);
  }

  @Test
  public void delete() {
    Assertions.assertThat(compte1).isNotNull();
    Virement virementAjoute = virementRepository.save(
            new Virement(new BigDecimal(1000), new Date(), compte1, compte2, "transfert"));
    List<Virement> virementsAvant = virementRepository.findAll();
    int sizeAvant = virementsAvant.size();
    Assertions.assertThat(virementsAvant).contains(virementAjoute);

    virementRepository.deleteById(virementAjoute.getId());

    List<Virement> virements = virementRepository.findAll();

    Assertions.assertThat(virements).doesNotContain(virementAjoute);
    Assertions.assertThat(virements.size()).isEqualTo(sizeAvant - 1);
  }
}