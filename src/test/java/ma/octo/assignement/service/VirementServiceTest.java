package ma.octo.assignement.service;

import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.model.Compte;
import ma.octo.assignement.model.Utilisateur;
import ma.octo.assignement.model.Virement;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VirementRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class VirementServiceTest {

    @Autowired
    private VirementService virementService;
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private VirementRepository virementRepository;

    private Compte compte1;
    private Compte compte2;

    @BeforeEach
    public void createMockData() throws Exception {
        Utilisateur u1 = new Utilisateur("ueService", "male", "test", "test", new Date());
        Utilisateur u2 = new Utilisateur("ubService", "male", "test", "test", new Date());
        Utilisateur user1 = utilisateurRepository.save(u1);
        Utilisateur user2 = utilisateurRepository.save(u2);
        Compte c1 = new Compte("nrEST", "ribEStest", new BigDecimal(50000), user1);
        Compte c2 = new Compte("nrBST", "ribBStest", new BigDecimal(0), user2);
        compte1 = compteRepository.save(c1);
        compte2 = compteRepository.save(c2);
    }

    @Test
    public void transactionSuccess(){
        BigDecimal solde2Avant = compte2.getSolde();
        BigDecimal solde1Avant = compte1.getSolde();
        VirementDto virementDto = new VirementDto();
        virementDto.setNrCompteEmetteur(compte1.getNrCompte());
        virementDto.setNrCompteBeneficiaire(compte2.getNrCompte());
        virementDto.setMotif("transfert");
        virementDto.setMontantVirement(new BigDecimal(2500));

        try {
            Virement virement = virementService.handleVirement(virementDto);
            Optional<Virement> virementOptional = virementRepository.findById(virement.getId());

            Assertions.assertThat(virementOptional.isEmpty()).isFalse();
            Virement virementDB = virementOptional.get();
            Assertions.assertThat(virementDB).isSameAs(virement);

            BigDecimal solde1Apres = compteRepository.findByNrCompte(compte1.getNrCompte()).getSolde();
            BigDecimal solde2Apres = compteRepository.findByNrCompte(compte2.getNrCompte()).getSolde();

            Assertions.assertThat(solde1Apres).isEqualTo(solde1Avant.subtract(virementDto.getMontantVirement()));
            Assertions.assertThat(solde2Apres).isEqualTo(solde2Avant.add(virementDto.getMontantVirement()));

        } catch (CompteNonExistantException | TransactionException | SoldeDisponibleInsuffisantException e) {
            e.printStackTrace();
            Assertions.fail("Exception ne doit pas être levée");
        }

    }

    @Test
    public void soldeInsuffisant(){
        VirementDto virementDto = new VirementDto();
        virementDto.setNrCompteEmetteur(compte2.getNrCompte());
        virementDto.setNrCompteBeneficiaire(compte1.getNrCompte());
        virementDto.setMotif("transfert");
        virementDto.setMontantVirement(new BigDecimal(3000));

        try {
            Virement virement = virementService.handleVirement(virementDto);
            Assertions.fail("Exception doit être levée.");
        } catch (CompteNonExistantException | TransactionException | SoldeDisponibleInsuffisantException e) {
            e.printStackTrace();
            Assertions.assertThat(e).isInstanceOf(SoldeDisponibleInsuffisantException.class);
        }

    }

    @Test
    public void montantMinimalNonAtteint(){
        VirementDto virementDto = new VirementDto();
        virementDto.setNrCompteEmetteur(compte1.getNrCompte());
        virementDto.setNrCompteBeneficiaire(compte2.getNrCompte());
        virementDto.setMotif("transfert");
        virementDto.setMontantVirement(new BigDecimal(9));

        try {
            Virement virement = virementService.handleVirement(virementDto);
            Assertions.fail("Exception doit être levée.");
        } catch (CompteNonExistantException | TransactionException | SoldeDisponibleInsuffisantException e) {
            e.printStackTrace();
            Assertions.assertThat(e).isInstanceOf(TransactionException.class);
        }
    }

    @Test
    public void montantMaximalDepasse(){
        VirementDto virementDto = new VirementDto();
        virementDto.setNrCompteEmetteur(compte1.getNrCompte());
        virementDto.setNrCompteBeneficiaire(compte2.getNrCompte());
        virementDto.setMotif("transfert");
        virementDto.setMontantVirement(new BigDecimal(10001));

        try {
            Virement virement = virementService.handleVirement(virementDto);
            Assertions.fail("Exception doit être levée.");
        } catch (CompteNonExistantException | TransactionException | SoldeDisponibleInsuffisantException e) {
            e.printStackTrace();
            Assertions.assertThat(e).isInstanceOf(TransactionException.class);
        }
    }

    @Test
    public void motifVide(){
        VirementDto virementDto = new VirementDto();
        virementDto.setNrCompteEmetteur(compte1.getNrCompte());
        virementDto.setNrCompteBeneficiaire(compte2.getNrCompte());
        virementDto.setMotif("");
        virementDto.setMontantVirement(new BigDecimal(5000));

        try {
            Virement virement = virementService.handleVirement(virementDto);
            Assertions.fail("Exception doit être levée.");
        } catch (CompteNonExistantException | TransactionException | SoldeDisponibleInsuffisantException e) {
            e.printStackTrace();
            Assertions.assertThat(e).isInstanceOf(TransactionException.class);
        }
    }

}
