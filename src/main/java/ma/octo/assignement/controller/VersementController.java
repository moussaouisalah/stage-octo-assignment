package ma.octo.assignement.controller;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.model.Versement;
import ma.octo.assignement.service.VersementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/versements")
public class VersementController {

    @Autowired
    private VersementService versementService;

    @GetMapping
    List<Versement> loadAll(){
        return versementService.getAllVersements();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Versement createVersement(@RequestBody VersementDto versementDto)
            throws CompteNonExistantException, TransactionException {
        return versementService.handleVersement(versementDto);
    }

}
