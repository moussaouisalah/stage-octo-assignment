package ma.octo.assignement.repository;

import ma.octo.assignement.model.Operateur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OperateurDetailsRepository extends JpaRepository<Operateur, Long> {
    Operateur findByUsername(String username);
}
