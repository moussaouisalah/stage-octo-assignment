package ma.octo.assignement.repository;

import ma.octo.assignement.model.Versement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VersementRepository extends JpaRepository<Versement, Long> {
}
