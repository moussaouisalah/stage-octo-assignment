package ma.octo.assignement.service;

import ma.octo.assignement.model.Audit;
import ma.octo.assignement.model.util.EventType;
import ma.octo.assignement.repository.AuditRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AuditService {

    Logger LOGGER = LoggerFactory.getLogger(AuditService.class);

    @Autowired
    private AuditRepository auditRepository;

    private void audit(String message, EventType eventType) {

        LOGGER.info("Audit de l'événement {}", eventType);

        Audit audit = new Audit();
        audit.setEventType(eventType);
        audit.setMessage(message);
        auditRepository.save(audit);
    }

    public void auditVersement(String message){
        this.audit(message, EventType.VERSEMENT);
    }

    public void auditVirement(String message){
        this.audit(message, EventType.VIREMENT);
    }
}
