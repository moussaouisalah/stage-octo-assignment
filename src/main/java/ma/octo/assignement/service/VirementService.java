package ma.octo.assignement.service;

import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.model.Compte;
import ma.octo.assignement.model.Virement;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class VirementService {

    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(VirementService.class);

    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private VirementRepository virementRepository;
    @Autowired
    private AuditService auditService;

    public List<Virement> getAllVirements() {
        return virementRepository.findAll();
    }

    public Virement handleVirement(VirementDto virementDto)
            throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {
        Compte compteEmetteur = compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur());
        Compte compteBeneficiaire = compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire());

        if (compteEmetteur == null) {
            LOGGER.error("Compte Emetteur Non existant");
            throw new CompteNonExistantException("Compte Emetteur Non existant");
        }

        if (compteBeneficiaire == null) {
            LOGGER.error("Compte Bénéficiaire Non existant");
            throw new CompteNonExistantException("Compte Bénéficiaire Non existant");
        }

        if (virementDto.getMontantVirement() == null ||
        virementDto.getMontantVirement().compareTo(BigDecimal.ZERO) <= 0) {
            LOGGER.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (virementDto.getMontantVirement().compareTo(BigDecimal.TEN) < 0) {
            LOGGER.error("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (virementDto.getMontantVirement().compareTo(BigDecimal.valueOf(MONTANT_MAXIMAL)) > 0) {
            LOGGER.error("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }

        if (virementDto.getMotif() == null || virementDto.getMotif().isEmpty()) {
            LOGGER.error("Motif vide");
            throw new TransactionException("Motif vide");
        }

        BigDecimal newSoldeEmetteur = compteEmetteur.getSolde().subtract(virementDto.getMontantVirement());
        if (newSoldeEmetteur.compareTo(BigDecimal.ZERO) < 0) {
            LOGGER.error("Solde insuffisant pour l'émetteur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'émetteur");
        }

        compteEmetteur.setSolde(newSoldeEmetteur);

        BigDecimal newSoldeBeneficiaire = compteBeneficiaire.getSolde().add(virementDto.getMontantVirement());
        compteBeneficiaire.setSolde(newSoldeBeneficiaire);

        Virement virement = new Virement();
        virement.setDateExecution(new Date());
        virement.setCompteBeneficiaire(compteBeneficiaire);
        virement.setCompteEmetteur(compteEmetteur);
        virement.setMontantVirement(virementDto.getMontantVirement());
        virement.setMotifVirement(virementDto.getMotif());

        compteRepository.save(compteEmetteur);
        compteRepository.save(compteBeneficiaire);
        Virement virementCree = virementRepository.save(virement);

        auditService.auditVirement("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
                .getNrCompteBeneficiaire() + " d'un montant de " + virementDto.getMontantVirement()
                .toString());

        return virementCree;
    }

}
