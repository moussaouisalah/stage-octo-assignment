package ma.octo.assignement.service;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.model.Compte;
import ma.octo.assignement.model.Versement;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import org.hibernate.tool.schema.spi.CommandAcceptanceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class VersementService {

    Logger LOGGER = LoggerFactory.getLogger(VersementService.class);

    @Autowired
    private VersementRepository versementRepository;
    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private AuditService auditService;

    public List<Versement> getAllVersements() {
        return versementRepository.findAll();
    }

    public Versement handleVersement(VersementDto versementDto) throws TransactionException, CompteNonExistantException {
        Compte compteBeneficiaire = compteRepository.findByRib(versementDto.getRibCompte());

        if(compteBeneficiaire == null){
            LOGGER.error("Compte Bénéficiaire non existant");
            throw new CompteNonExistantException("Compte Bénéficiaire Non existant");
        }

        if(versementDto.getMontantVersement() == null ||
        versementDto.getMontantVersement().compareTo(BigDecimal.ZERO) <= 0){
            LOGGER.error("Montant vide");
            throw new TransactionException("Montant vide");
        }

        if (versementDto.getMotif() == null || versementDto.getMotif().isEmpty()) {
            LOGGER.error("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (versementDto.getNomPrenomEmetteur() == null || versementDto.getNomPrenomEmetteur().isEmpty()) {
            LOGGER.error("Nom et prénom d'émetteur vide");
            throw new TransactionException("Nom et prénom d'émetteur vide");
        }

        BigDecimal newSoldeBeneficiaire = compteBeneficiaire.getSolde().add(versementDto.getMontantVersement());
        compteBeneficiaire.setSolde(newSoldeBeneficiaire);

        Versement versement = new Versement(
                versementDto.getMontantVersement(),
                new Date(),
                versementDto.getNomPrenomEmetteur(),
                compteBeneficiaire,
                versementDto.getMotif()
        );

        compteRepository.save(compteBeneficiaire);
        Versement versementCree = versementRepository.save(versement);

        auditService.auditVersement("Versement fait par " + versementDto.getNomPrenomEmetteur() + " en faveur de " +
                compteBeneficiaire.getNrCompte() + " d'un montant de " + versementDto.getMontantVersement().toString());

        return versementCree;
    }
}
