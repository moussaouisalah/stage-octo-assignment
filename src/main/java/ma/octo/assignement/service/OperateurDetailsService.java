package ma.octo.assignement.service;

import ma.octo.assignement.model.Operateur;
import ma.octo.assignement.repository.OperateurDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class OperateurDetailsService implements UserDetailsService {

    @Autowired
    OperateurDetailsRepository operateurDetailsRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Operateur operateur = operateurDetailsRepository.findByUsername(username);
        if(operateur == null)
            throw new UsernameNotFoundException("Operateur introuvable");
        return operateur;
    }
}
