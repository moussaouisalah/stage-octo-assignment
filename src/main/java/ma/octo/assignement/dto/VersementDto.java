package ma.octo.assignement.dto;

import java.math.BigDecimal;

public class VersementDto {
    private String ribCompte;
    private String nomPrenomEmetteur;
    private BigDecimal montantVersement;
    private String motif;

    public String getRibCompte() {
        return ribCompte;
    }

    public void setRibCompte(String ribCompte) {
        this.ribCompte = ribCompte;
    }

    public String getNomPrenomEmetteur() {
        return nomPrenomEmetteur;
    }

    public void setNomPrenomEmetteur(String nomPrenomEmetteur) {
        this.nomPrenomEmetteur = nomPrenomEmetteur;
    }

    public BigDecimal getMontantVersement() {
        return montantVersement;
    }

    public void setMontantVersement(BigDecimal montantVersement) {
        this.montantVersement = montantVersement;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }
}
