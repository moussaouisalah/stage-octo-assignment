package ma.octo.assignement.mapper;

import ma.octo.assignement.model.Virement;
import ma.octo.assignement.dto.VirementDto;

public class VirementMapper {

    public static VirementDto map(Virement virement) {
        VirementDto virementDto = new VirementDto();
        virementDto.setNrCompteEmetteur(virement.getCompteEmetteur().getNrCompte());
        virementDto.setMotif(virement.getMotifVirement());
        virementDto.setMontantVirement(virement.getMontantVirement());
        virementDto.setNrCompteBeneficiaire(virement.getCompteBeneficiaire().getNrCompte());

        return virementDto;
    }

}
